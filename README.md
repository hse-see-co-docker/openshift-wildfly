# Openshift Origin Wildfly S2I image [![build status](https://gitlab.cern.ch/hse-see-co-docker/openshift-wildfly/badges/master/build.svg)](https://gitlab.cern.ch/hse-see-co-docker/openshift-wildfly/commits/master)

## Supported builders

* Maven `3.3.9`
* Gradle `3.4.1`

## Wildfly Application Server

Current version `10.1.0.Final`

* Disabled `server` and `X-powered-by` headers
* Added Oracle 8 Driver
* Exposed port `8080`
* Removed `weld` submodule